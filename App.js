import { createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
import Login from "./src/modules/login/containers/login";
import Register from "./src/modules/login/containers/table JSON";


const LoginNavigator = createStackNavigator({
  Login: {
    screen: Login,
    navigationOptions: {
      title: "JBooks",
      headerBackTitle: null, //Quita el titulo del boton para regresar a la pantalla anterior
      headerTitleStyle: { fontSize: 25 },
      headerLeft: null //Elimina el boton de retroceso
    }
  },
  Register: {
    screen: Register,
    navigationOptions: {
      title: "Issues",
      headerTitleStyle: { fontSize: 25 },
      headerLeft: null //Elimina el boton de retroceso
    }
  }
},
 { headerLayoutPreset: "center" }
 );

export default createAppContainer(LoginNavigator) 