import React, { Component } from "react";
import { StyleSheet } from "react-native";
import { Table, TableWrapper, Row, Col } from "react-native-table-component";


export default class issuesTable extends Component {
    render()
    {
        return(
            <Table borderStyle={{ borderWidth: 1 }}>
              <Row
                data={[
                  this.props.propsCheckboxAll,
                  "Task ID",
                  "Summary",
                  "Time Billed",
                  ""
                ]}
                flexArr={[0.7, 1.4, 3, 1.8, 0.7]}
                style={styles.head}
                textStyle={styles.negrita}
              />
              <TableWrapper style={styles.wrapper}>
                <Col
                  data={this.props.propsArrayCheckboxAll}
                  style={{ flex: 0.7 }}
                  heightArr={this.props.propsTotalisuessarray}
                  textStyle={styles.text}
                /> 
                <Col
                  data={this.props.propsIssuesTaskID}
                  style={{ flex: 1.4 }}
                  heightArr={this.props.propsTotalisuessarray}
                  textStyle={styles.text}
                />
                <Col
                  data={this.props.propsIssuesSummary}
                  style={{ flex: 3 }}
                  heightArr={this.props.propsTotalisuessarray}
                  textStyle={styles.text}
                />
                <Col
                  data={this.props.propsArrayTextInput}
                  style={{ flex: 1.8 }}
                  heightArr={this.props.propsTotalisuessarray}
                  textStyle={styles.text}
                />
                <Col
                  data={this.props.propsArrayDetails}
                  style={{ flex: 0.7 }}
                  heightArr={this.props.propsTotalisuessarray}
                  textStyle={styles.text}
                />
              </TableWrapper>
            </Table>
        )
    }
}


const styles = StyleSheet.create({
  head: { height: 40, backgroundColor: "#D3D1D1" },
  wrapper: { flexDirection: "row", backgroundColor: "#FFFEF6" },
  text: { textAlign: "center", color: "black" },
  negrita: {textAlign: "center", color: "black",fontWeight: 'bold'}
})