import React, { Component } from "react";
import { StyleSheet } from "react-native";
import { Table, TableWrapper, Row, Col } from "react-native-table-component";


export default class sendTable extends Component {
    render()
    {
        return(
            <Table borderStyle={{ borderWidth: 1 }}>
              <Row
                data={[
                  "Task ID",
                  "Summary",
                  "Time Billed"
                ]}
                flexArr={[1.4, 3, 1.8]}
                style={styles.head}
                textStyle={styles.negrita}
              />
              <TableWrapper style={styles.wrapper}>
                <Col
                  data={this.props.propsSendIssuesTaskID}
                  style={{ flex: 1.4 }}
                  heightArr={this.props.propsSendIssuessarray}
                  textStyle={styles.text}
                />
                <Col
                  data={this.props.propsSendIssuesSummary}
                  style={{ flex: 3 }}
                  heightArr={this.props.propsSendIssuessarray}
                  textStyle={styles.text}
                />
                <Col
                  data={this.props.propsSendArrayTextInput}
                  style={{ flex: 1.8 }}
                  heightArr={this.props.propsSendIssuessarray}
                  textStyle={styles.text}
                />
              </TableWrapper>
            </Table>
        )
    }
}


const styles = StyleSheet.create({
  head: { height: 40, backgroundColor: "#D3D1D1" },
  wrapper: { flexDirection: "row", backgroundColor: "#FFFEF6" },
  text: { textAlign: "center", color: "black" },
  negrita: {textAlign: "center", color: "black",fontWeight: 'bold'}
})