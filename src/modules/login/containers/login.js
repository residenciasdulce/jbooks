import React, { Component } from "react";
import {
  Container,
  Content,
  Card,
  CardItem,
  Text,
  Body,
  Button,
  Item,
  Input,
  Icon,
  View
} from "native-base";
import { StyleSheet } from "react-native";
import * as Font from "expo-font"; //Libreria para poder utilizar fuentes
import { Ionicons } from "@expo/vector-icons"; //Libreria para poder utilizar iconos

export default class CardItemBordered extends Component {
  state = {
    loading: true,
    showpassword: true,
    password: ""
  };

  //GRACIAS A ESTO, JALA LA FUENTE, Y NO LA DESCARGUE
  async componentDidMount() {
    await Font.loadAsync({
      Roboto: require("./Roboto.ttf"),
      Roboto_medium: require("./Roboto_medium.ttf"),
      ...Ionicons.font
    });
    this.setState({ loading: false });
  }
  //--------------

  //-------------------------------------------COMIENZA METODO REGISTER-----------------------------------------
  register = () => {
    let data = {
      method: "POST",
      body: JSON.stringify({
        email: this.state.email,//"user@example.com" //Le mando el email que coloque en el textbox
        password: this.state.password//"foobar" //Le mando la password que coloque en el textbox
      }),
      headers: { 
        Accept: "application/json",
        "Content-Type": "application/json"
      }
    };


    this.setState({
      isLoading: true
    });

    return fetch("http://52.10.106.188:6015/api/v1/authenticate", data) //URL de la API a la que nos queremos conectar (quite el nombre del localhost y coloque la IP del equipo para que funcione en android)
      .then(response => response.json())
      //Atrapamos lo que nos responde el JSON (responseJson)
      .then(responseJson => {
        this.setState({
          isLoading: false,
          data: responseJson.data
        });

        var error = { 
          "error": {
          "user_authentication": [
              "invalid credentials"
          ]
      } 
    }; //Esto nos devuelve el POST cuando no encuentra las credenciales en la API

        //CONDICION PARA INGRESAR A LA PANTALLA DE LA TABLA DESPUES DE DAR CLIC EN EL BOTON DEL LOGIN
        if (JSON.stringify(responseJson) == JSON.stringify(error)) {
          alert("Invalid Credentials"); //Esto aparecera si no encuentra las credenciales
        } else {
          this.setState({password: ""}) //Borra la pass cuando se cierre la sesion
          var token = JSON.stringify(responseJson).substring(15,120)
          this.props.navigation.navigate("Register",{token: token}); //Permite cambiar a la ventana "Register", envio token
        }
      })
      .catch(error => {
        console.error(error);
      });
    //
  };
//-------------------------------------------TERMINA METODO REGISTER-----------------------------------------
  
render() {
    //ESTO TAMBIEN AYUDO EN LA FUENTE
    // if (this.state.loading) {
    //   return <View></View>;
    // }
    //--------------------
    return (
      <Container>
        <Content padder contentContainerStyle={styles.content}>
          <Card>
            <CardItem header bordered>
              <Text style={styles.TextCenter}>Sign Up</Text>
            </CardItem>
            <CardItem bordered>
              <Body style={styles.body}>
                <Item stackedLabel>
                  <Icon active name="person"></Icon>
                  <Input
                    placeholder="Email" 
                    onChangeText={email => this.setState({ email })} //con esta propiedad podemos tomar el valor de email para mandarlo a la api
                  />
                </Item>
                <Item floatingLabel last>
                  <Icon active name="ios-key"></Icon>
                  <Input
                    placeholder="Password" 
                    value={this.state.password}
                    secureTextEntry={true} //Debe ser true para ocultar la pass
                    onChangeText={password => this.setState({ password })} //con esta propiedad podemos tomar el valor de pass para mandarlo a la api
                  />
                </Item>
              </Body>
            </CardItem>
            <CardItem footer bordered>
              {/*Al dar clic en el boton, se dispara el metodo register*/}
              <Button bordered style={styles.button} onPress={this.register}> 
                <Text style={{fontSize: 20}}>Login</Text>
              </Button>
            </CardItem>
          </Card>
        </Content>
      </Container>
    );
  }
}
const styles = StyleSheet.create({
  TextCenter: {
    textAlign: "center",
    width: "100%",
    fontSize: 40
  }, //Centra el cuadro del login
  content: {
    flex: 1,
    justifyContent: "center"
  }, //Coloca el boton lo mas pegado a la esquina inferior derecha
  button: {
    marginLeft: "38%"
  }, //separacion entre form y boton
  body: {
    paddingVertical: 30
  }
});
