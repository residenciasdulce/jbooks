import React, { Component } from "react";
import { StyleSheet, ScrollView, Alert, TextInput, View } from "react-native";
import { Content, Text, Button, Footer, Icon, Spinner, Header } from "native-base";
import * as Font from "expo-font";
import { Ionicons, AntDesign } from "@expo/vector-icons";
import { Dialog } from "react-native-simple-dialogs"; //Descargada
import DatePicker from "react-native-datepicker"; //Descargada
import IssuesTable from "../components/IssuesTable"; //Componente Tabla
import SendTable from "../components/SendTable"; //Componente Tabla a enviar
import { Dropdown } from "react-native-material-dropdown";
import CheckBox from "react-native-checkbox";
import '/Users/dulcetijerina/Desktop/native/src/modules/login/containers/globalArray.js'
import moment from 'moment';
export default class DeckSwiperAdvancedExample extends Component {
  //Funcion que nos abre la ventana de dialogo Send All
  openDialogSendAll = show => {
    this.setState({ showDialogSendAll: show });
  };
  //Funcion que nos abre la ventana de dialogo Filters
  openDialog = show => {
    this.setState({ showDialog: show });
  };
  //Funcion que nos abre la ventana de dialogo filters
  openDialogSend = show => {
    this.setState({ showDialogSend: show });
  };
  //Funcion que nos abre la ventana de dialogo 
  openDialogExit = show => {
    
    if(show === true || show === false)
    {
      this.setState({ showDialogExit: show }) 
    }
    else
    {
      this.setState({ showDialogExit: false }) 
      this.props.navigation.navigate('Login');
    }
  };
  //---------------------------------------
  constructor(
    props //sobreescribimos el constructor
  ) {
    super(props); //Hace referencia al constructor de la clase base (React.Component), puedes usar this despues de esta linea.

    this.state = {
      loading: false, //Con esto podremos saber cuando la aplicacion se encuentre en proceso de descarga (aun no lo esta)
      resultsAPI: [], //aqui almacenaremos todos los pokemon que nos retorne el API
      urlIssuesAll: "http://52.10.106.188:6015/api/v1/issues", //URL de la API
      urlFilters: "http://52.10.106.188:6015/api/v1/search",
      token: this.props.navigation.state.params.token,
      dateInit: "",
      dateFinal: "",
      vendor: "",
      projectKey: "",
      buttonSend: true,
      buttonSendAll: false
    };
  }
  //Funcion que genera todos los checkbox
  elementCheckboxAll = i => (
    <CheckBox
      label=""
      checkboxStyle={{marginLeft:3.5 }} //Centra el checkbox
      onChange={checked => checkboxfuncion(checked, i)}
    />
    //<Switch onSyncPress={(checked) => checkboxfuncion(checked,i)}/>
  );

  //Funcion que genera un icono para la columna "Details"
  elementDetailsAll = (
    positionColumnDetails //Recibo la posicion de cada celda.
  ) => (
    <Icon
      name="md-information-circle-outline"
      onPress={() => detailsAlert(positionColumnDetails)}
      style={styles.details}
    /> //cuando se de clic en el icono, se mandara la posicion de la celda donde se dio el clic
  );
  //Funcion que genera un textInput para la columna ""Time Billed" y le agrega su valor (timeSpent)
  elementTimeBilledAll = (timeBilled,i //Recibo el timeSpent de cada celda con su posicion
    ) => (
    <TextInput style={{ textAlign: "center" }} 
    multiline
        numberOfLines={2}
    maxLength={20}
    onChangeText={(text) => 
    metodoElementosEditados(text,i)
    }>
    {timeBilled}</TextInput> //Asigno valor a cada textInput
  );

  //Esta funcion se ejecuta inmediatamente despues de que los componentes hayan sido montados
  async componentDidMount() {
    await Font.loadAsync({
      //Cargamos las fuentes que vamos a utilizar
      Roboto: require("./Roboto.ttf"),
      Roboto_medium: require("./Roboto_medium.ttf"),
      ...Ionicons.font
    });
    this.APIallIssuesConnection(); //Mandamos llamar a la funcion get Pokemon
  }
  //------------------FUNCION GET QUE ME DEVUELVE TODOS LOS ISSUES----------------------------
  APIallIssuesConnection = () => {
    data = {
      method: "GET",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
        Authorization: "Bearer " + this.state.token
      }
    };
    this.setState({ loading: true }); //La aplicacion se encuentra en modo descarga
    fetch(this.state.urlIssuesAll, data) //Funcion que nos permite conectarnos al API (puede recibir varios parametros)
      .then(respuesta => respuesta.json()) //Tomamos la respuesta que nos devuelve el server y la convertimos a JSON

      .then(respuesta => {
        this.setState({
          resultsAPI: respuesta,
          loading: false
        });

        var error = {
          error: {
            user_authentication: ["invalid credentials"]
          }
        }; //Esto nos devuelve el POST cuando no encuentra las credenciales en la API
        if (JSON.stringify(respuesta) === JSON.stringify(error)) {
          alert(JSON.stringify(respuesta)); //Esto aparecera si no encuentra las credenciales
        }
      });
      this.setState({ showDialog: false })
  };
  //------------------FIN DE LA FUNCION GET QUE ME DEVUELVE TODOS LOS ISSUES----------------------------

  //------------------FUNCION GET QUE FILTRA LOS ISSUES----------------------------
  APIfilterIssuesConnection = () => {
    dataFilters = {
      method: "GET",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
        Authorization: "Bearer " + this.state.token
      }
    };
    this.setState({ loading: true }); //La aplicacion se encuentra en modo descarga
    fetch(
      this.state.urlFilters +
        "?startdate=" +
        this.state.dateInit +
        "&enddate=" +
        this.state.dateFinal +
        "&vendor=" +
        this.state.vendor +
        "&project=" +
        this.state.projectKey,
      dataFilters
    ) //Funcion que nos permite conectarnos al API (puede recibir varios parametros)
      .then(respuesta => respuesta.json()) //Tomamos la respuesta que nos devuelve el server y la convertimos a JSON

      .then(respuesta => {
        this.setState({
          resultsAPI: respuesta,
          loading: false
        });

        var error = {
          error: {
            user_authentication: ["invalid credentials"]
          }
        }; //Esto nos devuelve el POST cuando no encuentra las credenciales en la API
        if (JSON.stringify(respuesta) === JSON.stringify(error)) {
          alert(JSON.stringify(respuesta)); //Esto aparecera si no encuentra las credenciales
        }
      });
    this.setState({ vendor: "" }); //Resetea el vendor antes de cerrar la ventana
    this.setState({ projectKey: "" }); //Resetea la projectKey antes de cerrar la ventana
    this.openDialog(false); //Cierra ventana de dialogo
    taskID = [] //Vacio los arreglos que se muestran en la tabla SEND
    summarySend = []
    timeBilledSend = []
    elemetosSeleccionados = []
    elementosEditados = []
    this.setState({buttonSend: true, buttonSendAll: false})
  };
  //------------------FIN DE LA FUNCION GET QUE FILTRA LOS ISSUES------------------------------
  //------------------FUNCION POST QUE ENVIA LAS TAREAS A QUICKBOOKS----------------------------
  sendQuickBooks = (tareasSelectQuickBooks) => {
    let data = {
      method: "POST",
      body: JSON.stringify(
        {
          issuesdata: [
                  tareasSelectQuickBooks       
          ]
      }
      ),
      headers: { 
        Accept: "application/json",
        "Content-Type": "application/json",
        Authorization: "Bearer " + this.state.token
      }
    };
    this.setState({
      isLoading: true
    });
    
    return fetch("http://52.10.106.188:6015/api/v1/bills", data) //URL de la API a la que nos queremos conectar (quite el nombre del localhost y coloque la IP del equipo para que funcione en android)
      .then(response => response.json())
      //Atrapamos lo que nos responde el JSON (responseJson)
      .then(responseJson => {
        this.setState({
          isLoading: false,
          data: responseJson.data
        });

        var error = { 
            "error": "Request authorization error"       
    }; //CONDICION PARA INGRESAR A LA PANTALLA DE LA TABLA DESPUES DE DAR CLIC EN EL BOTON DEL LOGIN
    if (JSON.stringify(responseJson) == JSON.stringify(error)) {
      alert("Unauthorized User"); //Esto aparecera si no encuentra las credenciales
    } else {
      alert("Issues Sent to Quickbooks");
    }
  })
  .catch(error => {
    console.error(error);
  });
  }
  //------------------FIN DE LA FUNCION POST QUE ENVIA LAS TAREAS A QUICKBOOKS----------------------------
  render() {
    const state = this.state; //Se necesita para que la tabla funcione.

    if (this.state.loading) {
      //Si estamos en el trabajo de descarga...
      return (
        <View style={StyleSheet.container}>
          {/*Muestra una animacion de Loading*/}
          <Spinner color='blue' style={{marginTop: 250}}/>
        </View>
      );
    }
    {
      /*Ya que termino de descargar los datos de la API...*/
    }
    {
      /*OBTENGO DEL JSON LOS VALORES DE TASK ID*/
    }
    var issuesTaskID = this.state.resultsAPI.map(function(item) {
      return item.id;
    });
    {
      /*OBTENGO DEL JSON LOS VALORES DE PROJECT KEY*/
    }
    var issuesProjectKey = this.state.resultsAPI.map(function(item) {
      return item.fields.project.key;
    });
    {
      /*OBTENGO DEL JSON LOS VALORES DE TASK KEY*/
    }
    var issuesTaskKey = this.state.resultsAPI.map(function(item) {
      return item.key;
    });
    {
      /*OBTENGO DEL JSON LOS VALORES DE SUMMARY*/
    }
    var issuesSummary = this.state.resultsAPI.map(function(item) {
      return item.fields.summary;
    });
    {
      /*OBTENGO DEL JSON TODA LA SECCION ASSIGNEE*/
    }
    var issuesAsignee = this.state.resultsAPI.map(function(item) {
      return item.fields.assignee;
    });
    {
      /*OBTENGO DEL JSON LOS VALORES DE TIME SPENT*/
    }
    var issuesTimeSpent = this.state.resultsAPI.map(function(item) {
      return item.fields.worklog.worklogs[0]; //El [0] es porque el dato que necesito viene dentro de un arreglo en el JSON
    });
    {
      /*OBTENGO DEL JSON LOS VALORES DEL AUTOR DEL WORKLOG*/
    }
    var issuesWorklogAuthor = this.state.resultsAPI.map(function(item) {
      return item.fields.worklog.worklogs[0].author; //El [0] es porque el dato que necesito viene dentro de un arreglo en el JSON
    });
    {
      /*ARREGLO DE CHECKBOX*/
    }
    var arrayCheckboxAll = [];
    for (var i = 0; i < issuesTaskKey.length; i++) {
      arrayCheckboxAll[i] = this.elementCheckboxAll(i);
    }
    checkboxfuncion = (checked, i) => {
      if (checked === true) {
        elemetosSeleccionados.push(i);
        this.setState({buttonSend: false})
        this.setState({buttonSendAll: true})
      }
      if (checked === false) {
        for (var y = 0; y < elemetosSeleccionados.length; y++) {
          if (elemetosSeleccionados[y] === i) {
            elemetosSeleccionados.splice(y, 1);
          }
          if(elemetosSeleccionados.length == 0)
          {
            this.setState({buttonSend: true})
            this.setState({buttonSendAll: false})
          }
        }
      }
      taskTdaEnviar(elemetosSeleccionados)
    };
    taskTdaEnviar = (arreglo) => {
      taskID = []
       summarySend = []
       timeBilledSend = []
        for(var i = 0; i < arreglo.length; i++)
        {
          taskID.push(issuesTaskKey[arreglo[i]])
          summarySend.push(issuesSummary[arreglo[i]])
          if(elementosEditados[arreglo[i]] === undefined)
          {
            //TimeSpent trae solo los datos, no es el arreglo de textInput, ya que ese es editable
            timeBilledSend[i] = (timeSpent[arreglo[i]])
          }
          else
          {
            timeBilledSend[i] = (elementosEditados[arreglo[i]])
          }  
        }
    }
    {
      /*ESTO ME DA EL TOTAL DE FILAS QUE NECESITO*/
    }
    var totalisuessarray = [];
    for (var i = 0; i < issuesTaskKey.length; i++) {
      totalisuessarray[i] = 180; //Es el tama;o de la altura de las filas
    }
    {
      /*ARREGLO DE ICONOS PARA LA COLUMNA DE DETAILS*/
    }
    var arrayDetails = [];
    for (var i = 0; i < issuesTaskKey.length; i++) {
      arrayDetails[i] = this.elementDetailsAll(i); //Ademas de agregar el icono a cada celda del arreglo, tambien le mando su posicion.
    }
    {
      /*ARREGLO DE TEXT INPUTS*/
    }
    var arrayTextInput = [];
    for (var i = 0; i < issuesTaskKey.length; i++) {
      var timeBilled = JSON.stringify(issuesTimeSpent[i].timeSpent).substring(
        1,
        JSON.stringify(issuesTimeSpent[i].timeSpent).length - 1
      ); //Guardo valor de cada Time Spent
      arrayTextInput[i] = this.elementTimeBilledAll(timeBilled,i); //Mando cada valor del timeSpent
    }
    {
      /*ARREGLO DE TIME SPENT*/
    }
    var timeSpent = [];
    for (var i = 0; i < issuesTaskKey.length; i++) {
      var timeBilled2 = JSON.stringify(issuesTimeSpent[i].timeSpent).substring(
        1,
        JSON.stringify(issuesTimeSpent[i].timeSpent).length - 1
      ); //Guardo valor de cada Time Spent
      timeSpent[i] = timeBilled2
    }
    //--------
    metodoElementosEditados =(text,i)=> {
      elementosEditados[i] = text //Arreglo que almacena los cambios, junto la con posicion en la que se hizo
      for (var x = 0; x < elemetosSeleccionados.length; x++) {
        if(elemetosSeleccionados[x] === i)
        {
          timeBilledSend[x] = text //Modifica en tiempo real el valor del timeBilled en Send cuando el checkbox ya esta seleccionado
        }
        
      }
      
    }
    tareasSeleccionadasparaQuickBooks = () => {
      for(var i = 0; i < taskID.length; i++) //Arreglo de tareas seleccionadas a enviar
      {
        for(var x = 0; x < issuesTaskKey.length; x++) //Arreglo de todas las tareas
        {
          if(taskID[i] === issuesTaskKey[x]) //Compara si la tareas a enviar se encuentra en todas las tareas
          {
            if(taskID.length === 1) //Si solo hay una tarea a enviar...
            {
              var tareasSelectQuickBooks =
              {
                id: issuesTaskID[x],
                key: issuesTaskKey[x],
                projectKey: issuesProjectKey[x],
                worklogId: issuesTimeSpent[x].id,
                assignee: issuesAsignee[x],
                //worklogAuthor: issuesWorklogAuthor[x],
                worklog: "30m",
                billIds: "",
                invoiceIds: ""
            }
            }
          }
        }
      }
      alert(JSON.stringify(tareasSelectQuickBooks))
      this.sendQuickBooks(tareasSelectQuickBooks)
    }
    
    //------------ALERTA QUE MUESTRA LOS DETALLES DE CADA ISSUE------------------------------------------------
    detailsAlert = positionColumnDetails => {
      //Recibo posicion de la celda a la que se le hizo clic al icono
      for (var i = 0; i < issuesTaskKey.length; i++) {
        if (issuesTaskKey[i] === issuesTaskKey[positionColumnDetails]) {
          //comparo cada Task ID con el TASK ID al que se le dio el clic (A traves del icono)
          var dateSpanish = JSON.stringify(
            issuesTimeSpent[i].created
          ).substring(1, 11); //Obtengo fecha en formato espa;ol
          var dateEnglish =
            dateSpanish.substring(5, 7) +
            "-" +
            dateSpanish.substring(8, 10) +
            "-" +
            dateSpanish.substring(0, 4); //Le doy formato a ingles
          var Asignee = "Unknown";

          try {
            if (issuesAsignee[i].displayName != null) {
              //Si no tiene a nadie asignado....
              Asignee = JSON.stringify(issuesAsignee[i].displayName).substring(
                1,
                JSON.stringify(issuesAsignee[i].displayName).length - 1
              );
            }
          } catch (error) {
            //Atrapa el error de que encuentra un null (Aunque eso ya esta validado)
          }
          //Muestra un Alert con los datos del JSON
          Alert.alert(
            "DETAILS\n\n",
            "ASSIGNEE:\n" +
              Asignee +
              "\n\n" +
              "WORKLOG AUTHOR:\n" +
              JSON.stringify(issuesWorklogAuthor[i].displayName).substring(
                1,
                JSON.stringify(issuesWorklogAuthor[i].displayName).length - 1
              ) +
              "\n\n" +
              "TIME SPENT:\n" +
              JSON.stringify(issuesTimeSpent[i].timeSpent).substring(
                1,
                JSON.stringify(issuesTimeSpent[i].timeSpent).length - 1
              ) +
              "\n\n" +
              "WORKLOGS DATE:\n" +
              //dateEnglish
              dateSpanish
          );
        }
      }
    };

    //------------FIN DE LA ALERTA QUE MUESTRA LOS DETALLES DE CADA ISSUE------------------------------------------------
     //-----INICIO DE LA FUNCION QUE OBTIENE EL RANGO DE FECHA ACTUAL Y SE ENVIA AL API----------------------------
    currentWeek = () => {  
    var monday = moment().day(1).format('YYYY-MM-DD'); //Lunes de la semana actual
    var sunday = moment().weekday(7).format('YYYY-MM-DD');//Domingo de la semana actual
    this.state.dateInit = monday
    this.state.dateFinal = sunday
    this.APIfilterIssuesConnection()//Los datos los mando al API
    }
   //-----FIN DE LA FUNCION QUE OBTIENE EL RANGO DE FECHA ACTUAL Y SE ENVIA AL API----------------------------
    return (
      <Content>
        <ScrollView>
          



            <View style={{flex: 1, flexDirection: 'row', justifyContent: 'space-around', marginTop: 10, marginBottom:10}}>
            <Button iconLeft block
            onPress={() => 
              this.openDialogExit(true)
              //this.props.navigation.navigate('Login')
            } 
            style={{width: 170}}>
              <Icon name='md-log-out' />
              <Text style={{ fontSize: 20 }}>Exit</Text></Button>
            <Button iconLeft block
              onPress={() => this.openDialog(true)}
              style={{
                width: 170
              }}
            >
<Icon name='ios-funnel' />
              <Text style={{ fontSize: 20 }}>Filters</Text>
            </Button>
            </View>
            <View style={{flex: 1, flexDirection: 'row', justifyContent: 'space-around'}}>
            <Button iconLeft block
            disabled={this.state.buttonSend} //ESTO DESHABILITA EL BOTON
            style={{width: 170}}
              onPress={() => 
                this.openDialogSend(true)}>
              <Icon name='ios-send' />
              <Text style={{ fontSize: 20 }}>Send</Text>
            </Button>
            <Button iconLeft block
            disabled={this.state.buttonSendAll}
            style={{width: 170}}
            onPress={() => 
              this.openDialogSendAll(true)}>
              <Icon name='md-checkbox-outline' />
              <Text style={{ fontSize: 20 }}>Send All</Text></Button>
            </View>


            <View style={styles.container}>
            {/*COMIENZA TABLA*/}

            <IssuesTable
              style={{ marginTop: 0 }}
              // propsCheckboxAll={
              //   <Icon
              //     type="MaterialIcons"
              //     name="select-all"
              //     //onPress={() => alert("Prueba")}
              //   />
              // }
              propsTotalisuessarray={totalisuessarray}
              propsArrayCheckboxAll={arrayCheckboxAll}
              propsIssuesTaskID={issuesTaskKey}
              propsIssuesSummary={issuesSummary}
              propsArrayTextInput={arrayTextInput}
              propsArrayDetails={arrayDetails}
            />
            {/*TERMINA TABLA*/}
          </View>
        </ScrollView>

        <Dialog
          visible={this.state.showDialog}
          onTouchOutside={() => this.setState({ showDialog: false })}
          //Redondea las orillas de la ventana de dialogo
          dialogStyle={{ borderRadius: 40 }}
        >
          <ScrollView>
            <Text style={styles.mainText}>FILTERS</Text>
            <View>
              <View style={styles.body}>
                <Text style={styles.textcenter}>Initial date: </Text>
                {/* CALENDARIOS FILTERS --------------(JSX)*/}

                <DatePicker
                  style={{ width: 250 }}
                  date={this.state.dateInit}
                  mode="date"
                  placeholder="Select Date"
                  format="YYYY-MM-DD"
                  minDate="1970-01-01"
                  maxDate="2030-01-01"
                  confirmBtnText="Confirm"
                  cancelBtnText="Cancel"
                  customStyles={{
                    dateIcon: {
                      position: "absolute",
                      left: 0,
                      top: 4,
                      marginLeft: 25
                    },
                    dateInput: {
                      marginLeft: 60,
                      marginRight:25,
                      borderColor: "black"
                    },
                    placeholderText: {
                      fontSize: 18,
                      color: "#787779"
                    }
                    // ... You can check the source to find the other keys.
                  }}
                  onDateChange={dateInit => {
                    this.setState({ dateInit: dateInit }); //con esta propiedad podemos tomar el valor de dateInit para mandarlo a la api
                  }}
                />
                <Icon
                  name="ios-close-circle-outline"
                  onPress={() => this.setState({ dateInit: "" })} //Resetea la fecha
                  style={{ marginTop: -35, marginLeft: 228 }}
                />
                {/* FIN CALENDARIOS FILTERS----------(JSX)*/}

                <Text style={styles.textcenter}>Final date: </Text>
                <DatePicker
                  style={{ width: 250 }}
                  date={this.state.dateFinal}
                  mode="date"
                  placeholder="Select Date"
                  format="YYYY-MM-DD"
                  minDate="1970-01-01"
                  maxDate="2030-01-01"
                  confirmBtnText="Confirm"
                  cancelBtnText="Cancel"
                  customStyles={{
                    dateIcon: {
                      position: "absolute",
                      left: 0,
                      top: 4,
                      marginLeft: 25
                    },
                    dateInput: {
                      marginLeft: 60,
                      marginRight:25,
                      borderColor: "black"
                    },
                    placeholderText: {
                      fontSize: 18,
                      color: "#787779"
                    }
                  }}
                  onDateChange={dateFinal => {
                    this.setState({ dateFinal: dateFinal }); //con esta propiedad podemos tomar el valor de email para mandarlo a la api
                  }}
                />
                <Icon
                  name="ios-close-circle-outline"
                  onPress={() => this.setState({ dateFinal: "" })} //Resetea la fecha
                  style={{ marginTop: -35, marginLeft: 228 }}
                />
                <Dropdown
                  containerStyle={{ marginTop: 10, textColor: "black" }}
                  label="Vendor:"
                  fontSize={20}
                  labelFontSize={20}
                  textColor="black"
                  onChangeText={vendor => {
                    this.setState({ vendor: vendor }); //con esta propiedad podemos tomar el valor de email para mandarlo a la api
                  }}
                  data={[
                    {
                      value: ""
                    },
                    {
                      value: "Lead One"
                    },
                    {
                      value: "Rosa Norzagaray"
                    },
                    {
                      value: "Shawn"
                    },
                    {
                      value: "Josh Allen"
                    },
                    {
                      value: "Guillermo Abneed Rodríguez Velázquez"
                    },
                    {
                      value: "Julio Ortiz"
                    },
                    {
                      value: "David McEldowney"
                    },
                    {
                      value: "Isaí San Miguel"
                    },
                    {
                      value: "Ari Alan Perez"
                    }
                  ]}
                />
                <Dropdown
                  containerStyle={{ marginTop: 0, textColor: "black" }}
                  label="Project Key:"
                  fontSize={20}
                  labelFontSize={20}
                  textColor="black"
                  onChangeText={projectKey => {
                    this.setState({ projectKey: projectKey }); //con esta propiedad podemos tomar el valor de email para mandarlo a la api
                  }}
                  data={[
                    {
                      value: ""
                    },
                    {
                      value: "VNEA"
                    },
                    {
                      value: "TRUD"
                    },
                    {
                      value: "SNP"
                    },
                    {
                      value: "PEOP"
                    },
                    {
                      value: "NH"
                    },
                    {
                      value: "DR"
                    },
                    {
                      value: "DMC"
                    },
                    {
                      value: "CM"
                    },
                    {
                      value: "ALL"
                    },
                    {
                      value: "ALK"
                    }
                  ]}
                />
                <Button iconLeft block danger style={{marginTop: 10}}
                onPress={() => currentWeek()}>
            <Icon name='ios-calendar' />
            <Text style={{fontSize: 20}}>Current Week</Text>
          </Button>
          <Button iconLeft block danger style={{marginTop: 10}}
                onPress={() => this.APIallIssuesConnection()}>
            <Icon name='md-clipboard' />
            <Text style={{fontSize: 20}}>All Tasks</Text>
          </Button>
              </View>
              {/* BOTONES DE FILTERS ----------(JSX)*/}
              <Footer style={styles.footer}>
                <Footer style={styles.buttonYes}>
                  <Button onPress={() => this.APIfilterIssuesConnection()}>
                    <Text style={styles.textButton}>Yes</Text>
                  </Button>
                </Footer>
                <Footer style={styles.buttonNo}>
                  <Button onPress={() => this.openDialog(false)}>
                    <Text style={styles.textButton}>No</Text>
                  </Button>
                </Footer>
              </Footer>

              {/* FIN BOTONES DE FILTERS ----------(JSX)*/}
            </View>
          </ScrollView>
        </Dialog>
        {/* Termina ventana de dialogo FILTERS ------------------------------------------(JSX)*/}
        <Dialog
          visible={this.state.showDialogSend}
          //title style="SEND"
          onTouchOutside={() => this.setState({ showDialogSend: false })}
          dialogStyle={{ borderRadius: 40 }}
        >
          <View>
            <ScrollView>
              {/* <Text>{elemetosSeleccionados.toString()}</Text>
              <Text>{taskID.toString()}</Text> */}
              <Text style={styles.mainText}>SEND</Text>
              <Text style={{fontSize: 20,textAlign: 'center',marginTop: 10,marginBottom:5}}>Are you sure?</Text>
              {/* TABLA --------------(JSX)*/}
              <SendTable
              //style={{ marginTop: 0 }}
              propsSendIssuesTaskID = {taskID}
              propsSendIssuesSummary = {summarySend}
              propsSendArrayTextInput = {timeBilledSend}
              propsSendIssuessarray = {totalisuessarray}
            />
              {/* FIN TABLA ----------(JSX)*/}
              {/* BOTONES DE SEND ----------(JSX)*/}
              <Footer style={styles.footer}>
                <Footer style={styles.buttonYes}>
                  <Button onPress={() => tareasSeleccionadasparaQuickBooks()}>
                    <Text style={styles.textButton}>Yes</Text>
                  </Button>
                </Footer>
                <Footer style={styles.buttonNo}>
                  <Button onPress={() => this.openDialogSend(false)}>
                    <Text style={styles.textButton}>No</Text>
                  </Button>
                </Footer>
              </Footer>
              {/* FIN BOTONES DE SEND ----------(JSX)*/}
            </ScrollView>
          </View>
        </Dialog>
        <Dialog
          visible={this.state.showDialogSendAll}
          //title style="SEND"
          onTouchOutside={() => this.setState({ showDialogSendAll: false })}
          dialogStyle={{ borderRadius: 40 }}
        >
          <View>
            <ScrollView>
              <Text style={styles.mainText}>SEND ALL</Text>
              <Text style={{fontSize: 20,textAlign: 'center',marginTop: 10,marginBottom:5}}>Are you sure?</Text>
              {/* BOTONES DE SEND ALL----------(JSX)*/}
              <Footer style={styles.footer}>
                <Footer style={styles.buttonYes}>
                  <Button>
                    <Text style={styles.textButton}>Yes</Text>
                  </Button>
                </Footer>
                <Footer style={styles.buttonNo}>
                  <Button onPress={() => this.openDialogSendAll(false)}>
                    <Text style={styles.textButton}>No</Text>
                  </Button>
                </Footer>
              </Footer>
              {/* FIN BOTONES DE SEND ALL----------(JSX)*/}
            </ScrollView>
          </View>
        </Dialog>
        <Dialog
          visible={this.state.showDialogExit}
          //title style="SEND"
          onTouchOutside={() => this.setState({ showDialogExit: false })}
          dialogStyle={{ borderRadius: 40 }}
        >
          <View>
            <ScrollView>
              <Text style={styles.mainText}>EXIT</Text>
              <Text style={{fontSize: 20,textAlign: 'center',marginTop: 15,marginBottom:5}}>Are you sure about that?</Text>
              {/* BOTONES DE EXIT ----------(JSX)*/}
              <Footer style={styles.footer}>
                <Footer style={styles.buttonYes}>
                <Button onPress={() => this.openDialogExit("ExitApp")}>
                    <Text style={styles.textButton}>Yes</Text>
                  </Button>
                </Footer>
                <Footer style={styles.buttonNo}>
                  <Button onPress={() => this.openDialogExit(false)}>
                    <Text style={styles.textButton}>No</Text>
                  </Button>
                </Footer>
              </Footer>
              {/* FIN BOTONES DE EXIT ----------(JSX)*/}
            </ScrollView>
          </View>
        </Dialog>
      </Content>
    );
  }
}

const styles = StyleSheet.create({
  container: { flex: 1, padding: 5, paddingTop: 15, backgroundColor: "#fff" },
  head: { height: 40, backgroundColor: "#D3D1D1" },
  wrapper: { flexDirection: "row", backgroundColor: "#FFFEF6" },
  title: { flex: 1, backgroundColor: "#f6f8fa" },
  row: { height: 28 },
  text: { textAlign: "center", color: "black" },
  button: { fontSize: 3 },
  details: { marginLeft: 5 },
  footer: {
    flex: 0.3,
    ...Platform.select({
      android: {
        marginTop: 15
      }
    })
  },
  buttonYes: {
    flex: 1,
    alignItems: "center",
    backgroundColor: "#E9E9EA",
    ...Platform.select({
      android: {
        backgroundColor: "white"
      }
    })
  },
  buttonNo: {
    flex: 1,
    alignItems: "center",
    backgroundColor: "#E9E9EA",
    ...Platform.select({
      android: {
        backgroundColor: "white"
      }
    })
  },
  textcenter: {
    fontSize: 20,
    marginTop: 15
  },
  body: {
    flex: 1
  },
  textButton:
  {
    fontWeight: "bold", 
    fontSize: 20,
    textAlign: 'center'
  },
  mainText: {
    fontSize: 25,
    color: "#1661C2",
    textAlign: 'center',
    fontWeight: "bold",
    marginTop: 15
  }
});
