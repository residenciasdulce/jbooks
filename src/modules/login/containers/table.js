import React, { Component } from 'react';
import * as Font from "expo-font";
import { Ionicons } from '@expo/vector-icons';
import { StyleSheet, ScrollView, Platform, TouchableOpacity, Alert } from 'react-native';
import { Container, View, Content, Card, CardItem, Text, Body, Button, Footer, CheckBox } from 'native-base';
import { Table, Row, Rows, Cell, TableWrapper } from 'react-native-table-component'; //Descargada
import { AntDesign } from '@expo/vector-icons'
import { Dialog } from "react-native-simple-dialogs"; //Descargada
import DatePicker from 'react-native-datepicker'; //Descargada


export default class MyComponent extends Component {
  //Funcion que nos abre la ventana de dialogo send
  Dialog = (show) => {
    this.setState({ dialogVisible: show });
  }
  //Funcion que nos abre la ventana de dialogo filters
  openDialog = (show) => {
    this.setState({ showDialog: show });
  }
  //---------------------------------------


  state = {
    loading: true
  }


  constructor(props) {
    super(props);

    this.state = {dateInit:"2016-05-15", dateFinal:"2016-05-15"}


    this.state = {
      tableHead: ['Head', 'Head2', 'Head3', 'Head4'],
      tableData: [
        ['1', '2', '3', '4'],
        ['a', 'b', 'c', 'd'],
        ['1', '2', '3', '4'],
        ['a', 'b', 'c', 'd']
      ]
    }
    }
  

   


  async componentDidMount() {
    await Font.loadAsync({
      'Roboto': require('./Roboto.ttf'),
      'Roboto_medium': require('./Roboto_medium.ttf'),
      ...Ionicons.font,
    })
    this.setState({ loading: false })
  }



  _alertIndex(index) {
    Alert.alert(`This is row ${index + 1}`);
  }



  render() {

    const state = this.state;
    const element = (data, index) => (
      <TouchableOpacity onPress={() => this._alertIndex(index)}>
        <View style={styles.btn}>
          <Text style={styles.btnText}>button</Text>
        </View>
      </TouchableOpacity>
    );


    // if (this.state.loading) {
    //   return (
    //     <View></View>
    //   );
    // }
    return (
      //--- Inicia Cuadro informativo para la tabla

      <Container>
        <ScrollView>
          <Content padder style={styles.mainbox}>
            <Card>
              <CardItem header bordered>
                <Text style={styles.mainText}>Main List</Text>
              </CardItem>
              <CardItem bordered>
                <Body>
                  <Text>
                    <AntDesign name='user' size={20}>  Username              </AntDesign>
                    <AntDesign name='pluscircleo' size={20}>  Key </AntDesign>
                  </Text>
                  <Text style={styles.row}>
                    <AntDesign name='key' size={20}>  ID                           </AntDesign>
                    <AntDesign name='filetext1' size={20}>  Summary</AntDesign>
                  </Text>
                </Body>
              </CardItem>
            </Card>
          </Content>
          {/* FIN CUADRO INFORMATIVO DE LA TABLA --------------(JSX)*/}
          {/* TABLA --------------(JSX)*/}
        

        {/* En vez de tabla estaba container*/}
          <View style={styles.container}>  
        <Table borderStyle={{borderWidth: 2, borderColor: '#021B4E'}}>
          <Row data={state.tableHead} style={styles.head} textStyle={styles.text}/>
          {
            state.tableData.map((rowData, index) => (
              <TableWrapper key={index} style={styles.row}>
                {
                  rowData.map((cellData, cellIndex) => (
                    //Esto coloca en la columna uno los botones
                    <Cell key={cellIndex} data={cellIndex === 0 ? element(cellData, index) : cellData} textStyle={styles.text}/>
                  ))
                }
              </TableWrapper>
            ))
          }
        </Table>
      </View>

          {/* FIN TABLA ----------(JSX)*/}

          {/* COMIENZA FOOTER ----------(JSX)*/}
          <Footer style={styles.footer}>
            <Footer style={styles.button1}>
              <Button onPress={() => this.openDialog(true)}><Text>Filters</Text></Button>
            </Footer>
            <Footer style={styles.button2}>
              <Button onPress={() => this.Dialog(true)}><Text>Send</Text></Button>
            </Footer>
          </Footer>
        </ScrollView>
        {/* TERMINA FOOTER ----------(JSX)*/}

        {/* Comienza ventana de dialogo SEND ----------(JSX)*/}
        
        <Dialog
        
          visible={this.state.dialogVisible}
          //title style="SEND"
          onTouchOutside={() => this.setState({ dialogVisible: false })} >
          <View>
            <ScrollView>
            <Text style={styles.mainText}>SEND</Text>
            <Text style={styles.textcenter}>Are you sure?</Text>
            {/* TABLA --------------(JSX)*/}
            
              <View style={styles.tabla}>
                <Table borderStyle={{ borderWidth: 2, borderColor: '#021B4E' }}>
                  <Row data={state.tableHead} style={styles.head} textStyle={styles.text} />
                  <Rows data={state.tableData} textStyle={styles.text} />
                </Table>
              </View>

              {/* FIN TABLA ----------(JSX)*/}
              {/* BOTONES DE SEND ----------(JSX)*/}
              <Footer style={styles.footer}>
                <Footer style={styles.button1}>
                  <Button><Text>Yes</Text></Button>
                </Footer>
                <Footer style={styles.button2}>
                  <Button onPress={() => this.Dialog(false)}><Text>No</Text></Button>
                </Footer>
              </Footer> 
            
            {/* FIN BOTONES DE SEND ----------(JSX)*/}
            
           </ScrollView> 
          </View>
           
        </Dialog>
       
        {/* Termina ventana de dialogo SEND ----------(JSX)*/}



        {/* Comienza ventana de dialogo FILTERS ----------(JSX)*/}
        <Dialog
        
          visible={this.state.showDialog}
          onTouchOutside={() => this.setState({ showDialog: false })} >         
            <ScrollView>
            <Text style={styles.mainText}>FILTERS</Text>
            <View>
            <View style={styles.body}>
            <Text style={styles.textcenter}>Initial date: </Text>
            {/* CALENDARIOS FILTERS --------------(JSX)*/}
            
            <DatePicker
        style={{width: 250}}
        date={this.state.dateInit}
        mode="date"
        placeholder="Select Date"
        format="YYYY-MM-DD"
        minDate="1970-01-01"
        maxDate="2030-01-01"
        confirmBtnText="Confirm"
        cancelBtnText="Cancel"
        customStyles={{
          dateIcon: {
            position: 'absolute',
            left: 0,
            top: 4,
            marginLeft: 25
          },
          dateInput: {
            marginLeft: 60,
            borderColor: 'black'
            
          },
          placeholderText: {
            fontSize: 16,
            color: '#787779'
        }
          // ... You can check the source to find the other keys.
        }}
        onDateChange={(dateInit) => {this.setState({dateInit: dateInit})}}
      />

              {/* FIN CALENDARIOS FILTERS----------(JSX)*/}

              <Text style={styles.textcenter}>Final date: </Text>
              <DatePicker
        style={{width: 250}}
        date={this.state.dateFinal}
        mode="date"
        placeholder="Select Date"
        format="YYYY-MM-DD"
        minDate="1970-01-01"
        maxDate="2030-01-01"
        confirmBtnText="Confirm"
        cancelBtnText="Cancel"
        customStyles={{
          dateIcon: {
            position: 'absolute',
            left: 0,
            top: 4,
            marginLeft: 25
          },
          dateInput: {
            marginLeft: 60,
            borderColor: 'black'
          },
          placeholderText: {
            fontSize: 16,
            color: '#787779'
        }
        }}
        onDateChange={(dateFinal) => {this.setState({dateFinal: dateFinal})}}
        
      />
      </View>
              {/* BOTONES DE FILTERS ----------(JSX)*/}
              <Footer style={styles.footer}>
                <Footer style={styles.button1}>
                  <Button><Text>Yes</Text></Button>
                </Footer>
                <Footer style={styles.button2}>
                  <Button onPress={() => this.openDialog(false)}><Text>No</Text></Button>
                </Footer>
              </Footer> 
            
            {/* FIN BOTONES DE FILTERS ----------(JSX)*/}

           
           </View> 
           </ScrollView> 
           
          
           
        </Dialog>


        {/* Termina ventana de dialogo FILTERS ----------(JSX)*/}



      </Container>
    )




  }
}


//ESTILOS
const styles = StyleSheet.create({
  tabla: {
    flex: 2.5, padding: 16, paddingTop: 5, backgroundColor: '#fff'
  },
  container: { flex: 1, padding: 16, paddingTop: 10, backgroundColor: '#fff' },
  //head: { height: 40, backgroundColor: '#808B97' },
  //text: { margin: 6 },
  row: { flexDirection: 'row'},
  btn: { width: 58, height: 18, backgroundColor: '#78B7BB',  borderRadius: 2 },
  btnText: { textAlign: 'center', color: '#fff' },
  head: { height: 40, backgroundColor: '#F0F3F9' },
  text: {margin: 6},
  mainText: {
    fontSize: 20,
    color: '#2C1697',
    marginLeft: 110,
    fontWeight: 'bold'
  },
  //row: {marginTop: 10},
  mainbox: {
    flex: 1
  },
  footer: {
    flex: 0.3,
    ...Platform.select({
      'android': {
        marginTop: 15
      }
    })

  },
  button1: {
    flex: 1,
    alignItems: "center",
    backgroundColor: '#E9E9EA',
    ...Platform.select({
      'android': {
        backgroundColor: "white"
      }
    })
  },
  button2: {
    flex: 1,
    alignItems: "center",
    backgroundColor: '#E9E9EA',
    ...Platform.select({
      'android': {
        backgroundColor: "white"
      }
    })
    
  },
  textcenter: {
    marginLeft: 100,
    fontSize: 20,
    marginTop: 15
  }, 
  body: {
    flex: 1,
    //backgroundColor: "#E9E9EA" 
  }
});